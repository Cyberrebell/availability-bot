<?php

use App\AvailabilityBot;
use App\TelegramClient;

chdir(__DIR__);
require 'vendor/autoload.php';

$config = json_decode(file_get_contents('config.json'), true);
$bot = new AvailabilityBot($config);
if (is_file('telegram.json')) {
    $telegramClient = new TelegramClient(json_decode(file_get_contents('telegram.json'), true));
    $bot->setTelegramClient($telegramClient);
}
$bot->run();