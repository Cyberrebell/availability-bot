<?php

namespace App;

use App\ShopClient\AlternateClient;
use App\ShopClient\AmazonClient;
use App\ShopClient\AmdClient;
use App\ShopClient\ArltClient;
use App\ShopClient\BoraComputerClient;
use App\ShopClient\ConradClient;
use App\ShopClient\CyberportClient;
use App\ShopClient\MediamarktClient;
use App\ShopClient\MindfactoryClient;
use App\ShopClient\NotebooksbilligerClient;
use App\ShopClient\SaturnClient;

class AvailabilityBot
{
    /* @var AbstractShopClient[] */
    protected array $clients;
    protected TelegramClient $telegramClient;

    protected array $searches;


    public function __construct(array $config)
    {
        $this->clients = [
            new AlternateClient(),
            new AmazonClient(),
            new AmdClient(),
            new ArltClient(),
            new BoraComputerClient(),
            new ConradClient(),
            new CyberportClient(),
            new MediamarktClient(),
            new MindfactoryClient(),
            new NotebooksbilligerClient(),
            new SaturnClient(),
        ];

        $this->searches = $config['searches'];
    }

    public function setTelegramClient(TelegramClient $telegramClient): void
    {
        $this->telegramClient = $telegramClient;
    }

    public function run(): void
    {
        while(true) {
            foreach ($this->searches as $search) {
                $searchClaim = $search['search-claim'];
                $priceLimit = $search['price-limit'] * 100;
                $claimBlacklist = $search['claim-blacklist'];

                $bulkMessage = '';
                foreach ($this->clients as $client) {
                    if (!$client->isReady()) {
                        continue;
                    }

                    $products = $client->findProducts($searchClaim, $priceLimit, $claimBlacklist);
                    if (!empty($products)) {
                        $message = $client->buildUrl($searchClaim) . PHP_EOL;

                        foreach ($products as $product) {
                            $message .= $product->render() . PHP_EOL;
                        }
                        echo $message .= PHP_EOL;
                        $bulkMessage .= $message;
                    }
                    echo '.';
                }
                if (isset($this->telegramClient) && !empty($bulkMessage)) {
                    $this->telegramClient->sendMessage($bulkMessage . implode(' ', $search['follower']));
                }
                echo PHP_EOL;
                sleep(20);
            }
        }
    }
}