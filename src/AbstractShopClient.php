<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;

abstract class AbstractShopClient
{
    const REQUEST_URL = '';
    const REQUEST_COOLDOWN = 200;
    const REQUEST_HEADERS = [
        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0',
        'Accept' => '*/*'
    ];
    const ACCEPTABLE_CLIENT_ERRORS = [
        443
    ];

    protected Client $client;
    protected int $lastRequestTime = 0;


    public function __construct()
    {
        $this->client = $this->createClient();
    }

    public function isReady(): bool
    {
        return $this->lastRequestTime + static::REQUEST_COOLDOWN < time();
    }

    /**
     * @return Product[]
     */
    public function findProducts(string $searchClaim, int $priceLimit, array $claimBlacklist): array
    {
        $products = [];
        try {
            $html = $this->request($searchClaim);
            if (empty($html)) {
                return $products;
            }
            foreach ($this->extractProducts($html) as $productHtml) {
                $product = $this->mapProduct($productHtml);
                if (!$product->available) {
                    continue;
                }
                if (!$product->titleContainsClaim($searchClaim)) {
                    continue;
                }
                foreach ($claimBlacklist as $claim) {
                    if ($product->titleContainsClaim($claim)) {
                        continue 2;
                    }
                }
                if ($product->price > $priceLimit) {
                    continue;
                }
                $products[] = $product;
            }
        } catch (ServerException $exception) {
            //ignore
        }

        $this->wasExecuted();
        return $products;
    }

    public static function buildUrl(string $searchClaim): string
    {
        return static::REQUEST_URL . urlencode($searchClaim);
    }

    abstract protected function extractProducts(string $responseContent): array;

    abstract protected function mapProduct(string $productContent): Product;

    protected function createClient(): Client
    {
        return new Client([
            'base_uri' => static::REQUEST_URL,
            'headers' => static::REQUEST_HEADERS,
            'verify' => false
        ]);
    }

    protected function request(string $searchClaim): string
    {
        return $this->failsaveGet(static::buildUrl($searchClaim));
    }

    protected function failsaveGet(string $url): string
    {
        try {
            return $this->client->request(
                'GET',
                $url
            )->getBody()->getContents();
        } catch (ConnectException $exception) {
            echo 'Connection to ' . $url . ' failed' . PHP_EOL;
            return '';
        } catch (ServerException $exception) {
            echo 'Error in ' . __CLASS__ . ' code: ' . $exception->getCode() . ' message: ' . $exception->getMessage() . ' Exception: ' . get_class($exception) . PHP_EOL;
            return '';
        } catch (RequestException $exception) {
            if (in_array($exception->getCode(), static::ACCEPTABLE_CLIENT_ERRORS, true)) {
                return ''; //ignore 4XX server errors
            }
            throw $exception;
        }
    }

    protected function wasExecuted(): void
    {
        $this->lastRequestTime = time();
    }
}