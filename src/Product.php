<?php

namespace App;

class Product
{
    public int $price;
    public string $title;
    public bool $available = true;


    public function cleanAndSetPrice(string $inputPrice): void
    {
        $price = str_replace(['.', ',', '–'], '', $inputPrice);
        preg_match('/\d+/', $price, $prices);
        $this->price = $prices[0];
    }

    public function titleContainsClaim(string $claim): bool
    {
        $words = explode(' ', $claim);
        foreach ($words as $word) {
            if (stripos($this->title, $word) === false) {
                return false;
            }
        }
        return true;
    }

    public function render(): string
    {
        $price = (float) $this->price;
        return number_format($price / 100, 2, ',', '.') . '€ - ' . utf8_encode($this->title);
    }
}