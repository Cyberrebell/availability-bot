<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class MindfactoryClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.mindfactory.de/search_result.php?search_query=';
    const REQUEST_COOLDOWN = 40000;


    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/<div class="pcontent">((.|\s)*)pinkl/U', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/pname">(.*)<\/div/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/pprice">(.*)<\/div/U', $productContent, $prices);
        $priceRaw = strip_tags($prices[1]);
        $priceRaw = str_replace('.', '', $priceRaw);
        $priceRaw = str_replace('-', '00', $priceRaw);
        preg_match('/\d+,\d+/', $priceRaw, $prices);
        $price = $prices[0];
        $product->cleanAndSetPrice($price);

        return $product;
    }
}