<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class WaveDistributionClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.wave-distribution.de/listing.xhtml?q=';


    protected function request(string $searchClaim): string
    {
        $response = $this->client->request(
            'GET',
            static::buildUrl($searchClaim)
        );
        preg_match('/=(.*);/U', $response->getHeader('Set-Cookie')[0], $matches);
        $jSessionId = $matches[1];

        return $this->client->request(
            'POST',
            'https://www.wave-distribution.de/mobile/listing.xhtml',
            [
                'form_params' => [
                    'lazyForm' => 'lazyForm',
                    'q' => $searchClaim,
                    'lazyComponent' => 'lazyListingContainer',
                    'javax.faces.ViewState' => 'stateless',
                    'javax.faces.source' => 'lazyButton',
                    'javax.faces.partial.event' => 'click',
                    'javax.faces.partial.execute' => 'lazyButton lazyButton',
                    'javax.faces.behavior.event' => 'action',
                    'javax.faces.partial.ajax' => 'true'
                ],
                'headers' => [
                    'Faces-Request' => 'partial/ajax',
                    'Origin' => 'https://www.wave-distribution.de',
                    'Referer' => 'https://www.wave-distribution.de/listing.xhtml?q=' . urlencode($searchClaim),
                    'Cookie' => 'JSESSIONID=' . $jSessionId
                ]
            ]
        )->getBody()->getContents();
    }

    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/productBox((.|\s)*)<\/div><\/a>/U', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/alt="(.*)"/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

//        preg_match('/price--default.*>((.|\s)*)<\/span>/U', $productContent, $prices);
//        $price = strip_tags($prices[1]);
//        $product->cleanAndSetPrice($price);
//
//        preg_match('/delivery-info-online(.|\s)*delivery--text-(.*)"/U', $productContent, $availability);
//        $product->available = $availability[2] === 'available';

        return $product;
    }
}