<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class MediamarktClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.mediamarkt.de/de/search.html?query=';


    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/fngalv(.*)dsMdSV/U', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/<p color=".*">(.*)<\/p>/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/<div tabindex.*">(.*)<\/div>/U', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $price = str_replace('–', '00', $price);
        $product->cleanAndSetPrice($price);

        return $product;
    }

    public static function buildUrl($searchClaim): string
    {
        return static::REQUEST_URL . rawurlencode($searchClaim);
    }
}