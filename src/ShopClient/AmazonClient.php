<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class AmazonClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.amazon.de/s?k=';


    protected function extractProducts(string $responseContent): array
    {
        $fragments = explode('a-price-symbol', $responseContent);
        unset($fragments[count($fragments) - 1]);
        foreach ($fragments as $key => $fragment) {
            $fragments[$key] = substr($fragment, strrpos($fragment, 's-product-image'));
        }

        return $fragments;
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/a-text-normal" dir="auto">(.*)<\/span>/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/a-offscreen">(.*)€<\/span>/U', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $product->cleanAndSetPrice($price);

        return $product;
    }

    public static function buildUrl($searchClaim): string
    {
        return static::REQUEST_URL . rawurlencode($searchClaim);
    }
}