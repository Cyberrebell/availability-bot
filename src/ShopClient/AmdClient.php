<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\InvalidResponseException;
use App\Product;
use GuzzleHttp\Exception\ServerException;

class AmdClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.amd.com/de/direct-buy/5458374100/de';


    public function findProducts(string $searchClaim, int $priceLimit, array $claimBlacklist): array
    {
        $products = [];
        $requestUrl = static::buildUrl($searchClaim);
        if (!empty($requestUrl)) {
            try {
                $curl = curl_init($requestUrl);
                curl_setopt_array($curl, [
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTPHEADER => static::REQUEST_HEADERS
                ]);
                $productHtml = curl_exec($curl);
                if (empty($productHtml)) {
                    return $products;
                }
                $product = $this->mapProduct($productHtml);
                if (!$product->available) {
                    return $products;
                }
                if (!$product->titleContainsClaim($searchClaim)) {
                    return $products;
                }
                if ($product->price > $priceLimit) {
                    return $products;
                }
                $products[] = $product;
            } catch (ServerException $exception) {
                //ignore
            } catch (InvalidResponseException $exception) {
                return $products;
            }

            $this->wasExecuted();
        }

        return $products;
    }

    public static function buildUrl(string $searchClaim): string
    {
        switch ($searchClaim) {
            case 'rx 6800 xt':
                return 'https://www.amd.com/de/direct-buy/5458374100/de';
            default:
                return '';
        }
    }

    protected function extractProducts(string $responseContent): array
    {
        //not needed
    }

    protected function mapProduct(string $productContent): Product
    {
        $reducedContent = substr($productContent, strpos($productContent, 'product-page-description'));
        $reducedContent = substr($reducedContent, 0, strpos($reducedContent, 'digital-river-specs'));
        if (empty($reducedContent)) {
            throw new InvalidResponseException();
        }

        $product = new Product();
        preg_match('/<h2>(.+)<\/h2>/', $reducedContent, $titles);
        $product->title = $titles[1];
        $product->title = str_replace('™', '', $product->title);

        preg_match('/<h4>(.+)<\/h4>/', $reducedContent, $prices);
        $product->cleanAndSetPrice($prices[1]);

        $product->available = strpos($reducedContent, 'Out of stock') === false;

        return $product;
    }
}