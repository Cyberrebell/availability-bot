<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class AlternateClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.alternate.de/html/search.html?size=500&query=';


    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/listRow">((.|\s)*)<span class="clear"><!-- e --><\/span>\s*<\/div>/U', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/class="name">(.*)/', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/class="price.*>((.|\s)*)<\/span/U', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $price = str_replace('-', '00', $price);
        $product->cleanAndSetPrice($price);

        $product->available = strpos($productContent, 'available_stock') !== false;

        return $product;
    }
}