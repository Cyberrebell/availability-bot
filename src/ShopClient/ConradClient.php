<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class ConradClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.conrad.de/de/search.html?search=';


    protected function request(string $searchClaim): string
    {
        $responseContent = parent::request($searchClaim);

        preg_match('/apikey=(.*)"/U', $responseContent, $apiKeys);

        $productsData = json_decode($this->client->request(
            'POST',
            'https://api.conrad.de/search/1/v3/facetSearch/de/de/b2c?apikey=' . $apiKeys[1],
            [
                'json' => [
                    'query' => $searchClaim,
                    'enabledFeatures' =>
                        [
                            'and_filters',
                            'filters_without_values',
                            'query_relaxation'
                        ],
                    'disabledFeatures' =>
                        [
                        ],
                    'globalFilter' =>
                        [
                        ],
                    'facetFilter' =>
                        [
                        ],
                    'sort' =>
                        [
                            [
                                'field' => '_score',
                                'order' => 'desc'
                            ],
                        ],
                    'from' => 0,
                    'size' => 30,
                    'facets' => [],
                    'partialThreshold' => 10,
                    'partialQueries' => 3,
                    'partialQuerySize' => 6
                ]
            ]
        )->getBody()->getContents(), true);

        $products = [];
        $articles = [];
        foreach ($productsData['hits'] as $item) {
            $articles[] = [
                'articleID' => str_pad($item['productId'], 18, '0', STR_PAD_LEFT),
                'calculatePrice' => true,
                'checkAvailability' => true,
                'findExclusions' => true,
                'insertCode' => '62'
            ];
            $products[$item['productId']] = $item['title'];
        }

        $details = json_decode($this->client->request(
            'POST',
            'https://api.conrad.de/price-availability/4/CQ_DE_B2C/facade?apikey=' . $apiKeys[1] . '&forceStorePrice=false',
            [
                'json' => [
                    'ns:inputArticleItemList' => [
                        '#namespaces' => [
                            'ns' => 'http://www.conrad.de/ccp/basit/service/article/priceandavailabilityservice/api'
                        ],
                        'articles' => $articles
                    ]
                ],
                'headers' => [
                    'Accept' => 'application/json, text/plain, */*'
                ]
            ]
        )->getBody()->getContents(), true);

        $realProducts = [];

        $detailsList = $details['priceAndAvailabilityFacadeResponse']['priceAndAvailability'];
        if (isset($detailsList['articleID'])) { //fix for "structure break if only one item" issue
            $detailsList = [$detailsList];
        }
        foreach ($detailsList as $detail) {
            $productId = ltrim($detail['articleID'], '0');
            if (isset($products[$productId])) {
                $realProducts[] = [
                    'title' => $products[$productId],
                    'price' => $detail['price']['price'],
                    'quantity' => $detail['availabilityStatus']['stockQuantity']
                ];
            }
        }
        return json_encode($realProducts);
    }

    protected function extractProducts(string $responseContent): array
    {
        $products = [];
        foreach (json_decode($responseContent, true) as $item) {
            $products[] = json_encode($item);
        }
        return $products;
    }

    protected function mapProduct(string $productContent): Product
    {
        $data = json_decode($productContent, true);

        $product = new Product();

        $product->title = $data['title'];

        $price = (float) $data['price'] * 100 . '';
        $product->cleanAndSetPrice($price);

        $product->available = $data['quantity'] > 0;

        return $product;
    }

    public static function buildUrl($searchClaim): string
    {
        return static::REQUEST_URL . rawurlencode($searchClaim);
    }
}