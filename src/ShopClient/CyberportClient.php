<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class CyberportClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.cyberport.de/tools/search-results.html?q=';


    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/<article(.+)<\/article>/Us', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/data-product-name="(.*)"/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/data-product-price="(.*)"/U', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $price = (float) $price * 100 . '';
        $product->cleanAndSetPrice($price);

        $product->available = strpos($productContent, 'Sofort verfügbar') !== false;

        return $product;
    }
}