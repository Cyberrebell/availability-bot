<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class BoraComputerClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.bora-computer.de/search?p=1&n=100&sSearch=';


    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/product--box((.|\s)*)buybox--form/U', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/product--title" title="(.*)">/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/price--default.*>((.|\s)*)<\/span>/U', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $product->cleanAndSetPrice($price);

        preg_match('/delivery-info-online(.|\s)*delivery--text-(.*)"/U', $productContent, $availability);
        $product->available = $availability[2] === 'available';

        return $product;
    }
}