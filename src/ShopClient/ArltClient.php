<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class ArltClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.arlt.com/index.php?cl=search&searchparam=';


    protected function extractProducts(string $responseContent): array
    {
        preg_match_all('/itemtype="http:\/\/schema\.org\/Product"((.|\s)*)tocart/U', $responseContent, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/<strong itemprop="name">(.*)/', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/class="price">(.*)/', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $product->cleanAndSetPrice($price);

        return $product;
    }
}