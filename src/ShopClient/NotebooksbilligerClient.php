<?php

namespace App\ShopClient;

use App\AbstractShopClient;
use App\Product;

class NotebooksbilligerClient extends AbstractShopClient
{
    const REQUEST_URL = 'https://www.notebooksbilliger.de/produkte/';
    const SESSION_ID = 'd79qgd0c6etmvndlkumng1fca4jq2f35rd1gq7bg1t7j17i5s3b3njoh1vgll2p4i1c8idga48u3dgh0s3uj0265b1rhghvapncecu9';


    protected function request(string $searchClaim): string
    {
        $searchUrl = 'https://search.epoq.de/inbound-servletapi/getSearchResult?tenantId=notebooksbilliger&format=json&query='
            . rawurlencode($searchClaim) . '&sessionId=' . static::SESSION_ID . '&orderBy=&order=desc&locakey=&style=onlyId&full&callback=jQuery18207105715738182745_1607810034633&&limit=10&offset=0&_=' . time();
        return $this->failsaveGet($searchUrl);
    }

    protected function extractProducts(string $responseContent): array
    {
        preg_match('/findings":(.*)}}/', $responseContent, $products);
        $requestUrl = 'https://www.notebooksbilliger.de/extensions/ntbde/getsearchlisting.php?pids=';
        foreach (json_decode($products[1], true)['finding'] as $finding) {
            if (isset($finding['match-item'])) {
                $finding = $finding['match-item'];
            }
            $requestUrl .= $finding['@node_ref'] . ',';
        }
        $productsData = $this->failsaveGet($requestUrl);

        preg_match_all('/product_name">((.|\s)*)<\/form>/U', $productsData, $products);
        return $products[1];
    }

    protected function mapProduct(string $productContent): Product
    {
        $product = new Product();

        preg_match('/title="(.*)"/U', $productContent, $titles);
        $product->title = strip_tags($titles[1]);

        preg_match('/data-price="(.*)"/U', $productContent, $prices);
        $price = strip_tags($prices[1]);
        $price = (float) $price * 100 . '';
        $product->cleanAndSetPrice($price);

        return $product;
    }
}