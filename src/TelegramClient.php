<?php

namespace App;

use GuzzleHttp\Client;

class TelegramClient
{
    protected Client $client;
    protected string $chatId;


    public function __construct(array $config)
    {
        $this->client = new Client([
            'base_uri' => 'https://api.telegram.org/bot' . $config['bot-token'] . '/'
        ]);
        $this->chatId = $config['chat-id'];
    }

    public function sendMessage(string $message): void
    {
        $this->client->request(
            'POST',
            'sendMessage',
            [
                'json' => [
                    'chat_id' => $this->chatId,
                    'text' => $message
                ]
            ]
        );
    }
}